import axios from 'axios';
import {v4 as uuidV4} from 'uuid';

const state = {
    todos: []
};

const getters = {
    allTodos: (state) => state.todos,
    pendingTodosCount: (state) => state.todos.filter(todo=>!todo.completed).length
};

const actions = {
    async fetchTodos({ commit }) {
        const { data } = await axios('https://jsonplaceholder.typicode.com/todos?_limit=8');
        commit('setTodos', data);
    },
    async addTodo({ commit }, title) {
        // const { data } = await axios.post('https://jsonplaceholder.typicode.com/todos', {
        //     title,
        //     completed: false,
        // });
        commit('newTodo', {
            id: uuidV4(),
            title,
            completed: false,
        });
    },
    async deleteTodoAction({ commit }, id) {
        // API is taking long time + CORS Issue
        // await axios.delete(`https://jsonplaceholder.typicode.com/todos?${id}`);
        commit('removeTodo', id);
    },
    async updateTodoAction({ commit }, todo) {
        // API is taking long time + CORS Issue
        // await axios.put(`https://jsonplaceholder.typicode.com/todos/${todo.id}`, todo);
        commit('updateTodo', todo);
    }
};

const mutations = {
    setTodos: (state, todos) => (state.todos = todos),
    newTodo: (state, todo) => state.todos.unshift(todo),
    updateTodo: (state, newTodoData) => {
        const todoIndex = state.todos.findIndex(todo => todo.id === newTodoData.id);
        if (todoIndex !== -1) {
            state.todos.splice(todoIndex, 1, newTodoData);
        }
    },
    removeTodo: (state, id) => (state.todos = state.todos.filter(todo => todo.id !== id)),
};

export default {
    state,
    getters,
    actions,
    mutations
}